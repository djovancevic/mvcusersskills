import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { UsersTableComponent } from './users/users-table/users-table.component';
import { SkillsTableComponent } from './users/skills-table/skills-table.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { AddSkillComponent } from './users/add-skill/add-skill.component';
import { DetailsComponent } from './users/details/details.component';
import { RegFormComponent } from './registration/reg-form/reg-form.component';
import { HomeComponent } from './home/home.component';
import { FilterComponent } from './users/filter/filter.component';
import { PagingComponent } from './users/paging/paging.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpConfigInterceptor} from './interceptor/httpconfig.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './registration/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material';
import { NotifierModule, NotifierOptions, NotifierService } from 'angular-notifier';
import { HttpErrorInterceptor } from './interceptor/http-error.interceptor';
import { Router } from '@angular/router';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { SkillDetailComponent } from './users/skill-detail/skill-detail.component';
import {APP_BASE_HREF} from '@angular/common';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators'

/**
 * Custom angular notifier options
 */
const customNotifierOptions : NotifierOptions = {
  position: {
		horizontal: {
			position: 'right',
			distance: 100
		},
	  vertical: {
			position: 'top',
			  distance: 240,
			  gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    UsersTableComponent,
    SkillsTableComponent,
    AddUserComponent,
    AddSkillComponent,
    DetailsComponent,
    RegFormComponent,
    HomeComponent,
    FilterComponent,
    PagingComponent,
    LoginComponent,
    SkillDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatDialogModule,
    NotifierModule,
    NotifierModule.withConfig(customNotifierOptions),
    ShowHidePasswordModule,
    RxReactiveFormsModule
    
  ],
  providers: [
    { provide : HTTP_INTERCEPTORS, useClass : HttpConfigInterceptor, multi : true},
    { provide : HTTP_INTERCEPTORS, useClass : HttpErrorInterceptor, multi : true, deps: [NotifierService, Router]},
    {provide: APP_BASE_HREF, useValue : '/' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
