import { Component, OnInit } from '@angular/core';
import { Credentials } from '../users/model/credentials';
import { AuthenticationService } from '../users/service/authentication.service'
import { Router } from '@angular/router';

@Component({
  selector: 'us-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser : any;
  cU : string;
  flag : boolean = false;
  constructor(private authenticationService : AuthenticationService, private router : Router ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //console.log("currentUser from localStorage je:  ", this.currentUser);
    
    if(this.currentUser){
      this.flag = true;
      this.cU = this.currentUser.userName;
      console.log("cU je:  ", this.cU);
    }
   }

  ngOnInit() {
    
  }

  onLogout(event :Event){
    this.router.navigate(['login']);
  }

}
