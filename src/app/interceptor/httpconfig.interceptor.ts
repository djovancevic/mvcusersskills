import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor() { }

  // function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
        // treba iskljuciti interceptor za login, register i get http methods
        // let isRegisterRequest : boolean = request.url.endsWith('/register');
        //const isPhoto: boolean = request.url.endsWith('.jpg');

        // if( !isRegisterRequest && !isLoginRequest ){
            // how to update the request Parameters
        let token: string;
        const currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
                token = currentUser.access_token;
            } else {token = ''; }

        console.log('Token dobavljen sa localStorage.getItem:   ', token);
        if (token) {
                request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
            }

        // if (!request.headers.has('Content-Type')) {
        //         if (!isPhoto) {
        //             //request = request.clone({ headers: request.headers.set('Content-Type', 'multipart/form-data') }); 
        //             request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        //         } else {
        //             //request = request.clone({ headers: request.headers.set('Content-Type', 'undefined') });
        //             request = request.clone({ headers: request.headers.set('Content-Type', '') });
        //         }
        //     }
        // if (!isPhoto) {
        //         request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        //     }
            // logging the updated Parameters to browser's console
        console.log('Before making api call : ', token);

        return next.handle(request).pipe(
                tap(
                    event => {
                        // logging the http response to browser's console in case of a success
                        if (event instanceof HttpResponse) {
                        console.log('api call success :', event);
                        }
                    },
                    error => {
                        // logging the http response to browser's console in case of a failuer
                        if (event instanceof HttpResponse) {
                        console.log('api call error :', event);
                        }
                    }
                )
            );
        // }
    }
}
