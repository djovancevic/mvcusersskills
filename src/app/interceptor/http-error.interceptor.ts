import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
   } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';

export class HttpErrorInterceptor implements HttpInterceptor {

    constructor( private notifierService: NotifierService, private router: Router) {
        
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request)
        .pipe(
          retry(1),
          catchError((error: HttpErrorResponse) => {
            let errorMessage = '';
            if (error.error instanceof ErrorEvent) {
              // client-side error
               errorMessage = `Error: ${error.error.message}`;
              // tslint:disable-next-line:no-trailing-whitespace
               this.notifierService.notify('error', errorMessage ); 
            } else {
              // server-side error
              errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
              if (error.status === 401) {
                this.notifierService.notify('error', 'Not authorized for this Action. You have to log in first.' );
                this.router.navigate(['login']);
              } else {
                this.notifierService.notify('error', errorMessage );
              }

            }
            // window.alert(errorMessage);
            return throwError(errorMessage);
          })
        );
    }
   }
