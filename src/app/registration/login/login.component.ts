import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

import { AuthenticationService } from '../../users/service/authentication.service';

@Component({
  selector: 'us-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  private notifier: NotifierService;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private notifierService: NotifierService
      ) {
          this.notifier = notifierService;
      } //private alertService: AlertService

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });

      // reset login status
      this.authenticationService.logout();

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.loading = true;
      this.authenticationService.login(this.f.username.value, this.f.password.value)
          .pipe(first())
          .subscribe(
              data => {
                  this.notifier.notify('success', 'User log in successful.');
                  this.router.navigate(["home"]);
              },
              error => {
                  //this.alertService.error(error);
                  //console.log('Greska prilikom autentikacije na serveru.');
                  this.notifier.notify('error', 'Greska prilikom autentikacije na serveru.');
                  this.loading = false;
              });
  }

  emailValidate(notyType: string, notyMessage: string): void {
    if (this.loginForm.get('username').status === 'VALID') {
      this.notifier.notify('success', 'Email is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }

  passwordValidate(notyType: string, notyMessage: string): void {
    if (this.loginForm.get('password').status === 'VALID') {
      this.notifier.notify('success', 'Password is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }
}
