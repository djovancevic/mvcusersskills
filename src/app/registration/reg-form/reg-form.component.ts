import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../users/service/users.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'us-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.css']
})
export class RegFormComponent implements OnInit {

  registerUserForm :FormGroup;
  loading = false;
  submitted = false;
  private notifier: NotifierService;

  	constructor(private fb: FormBuilder, private userService :UsersService, private router :Router, private notifierService: NotifierService) { 
      this.createForm();
      this.notifier = notifierService;
  	}

  	createForm(){
  		this.registerUserForm = this.fb.group({
  			email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern('(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}')]],
        confirmPassword: ['', [Validators.required, Validators.pattern('(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}')]]
  		});
  	}

  	ngOnInit() {  }

  	onSubmit(){
      this.submitted = true;

        // stop here if form is invalid
        if (this.registerUserForm.invalid && !this.loading) {
            return;
        }

        this.loading = true;
        this.userService.registerUser(this.registerUserForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    //this.alertService.success('Registration successful', true);
                    this.notifier.notify('success', 'User is successfuly registered.');
                    this.router.navigate(['/login']);
                },
                error => {
                    //this.alertService.error(error);
                    //console.log("Greska pri registraciji korisnika.");
                    this.notifier.notify('error', 'Greska prilikom registracije na serveru.');
                    this.loading = false;
                });
    }

    emailValidate(notyType: string, notyMessage: string): void {
      if (this.registerUserForm.get('email').status === 'VALID') {
        this.notifier.notify('success', 'Email is entered correctly');
      } else {
        this.notifier.notify(notyType, notyMessage);
      }
    }
  
    passwordValidate(notyType: string, notyMessage: string): void {
      if (this.registerUserForm.get('password').status === 'VALID') {
        this.notifier.notify('success', 'Password is entered correctly');
      } else {
        this.notifier.notify(notyType, notyMessage);
      }
    }
    
    confirmPasswordValidate(notyType: string, notyMessage: string): void {
      if (this.registerUserForm.get('confirmPassword').status === 'VALID') {
        if(this.registerUserForm.get('confirmPassword').value === this.registerUserForm.get('password').value){
          this.loading = true;
          this.notifier.notify('success', 'Password is confirmed correctly');
        }else{
          this.notifier.notify('warning', 'ConfirmPassword must match password.');
        }
        
      } else {
        this.notifier.notify(notyType, notyMessage);
      }
    }

}
