import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersTableComponent } from './users/users-table/users-table.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { DetailsComponent } from './users/details/details.component';
import { SkillsTableComponent } from './users/skills-table/skills-table.component';
import { AddSkillComponent } from './users/add-skill/add-skill.component';
import { HomeComponent } from './home/home.component';
import { RegFormComponent } from './registration/reg-form/reg-form.component';
import { LoginComponent } from './registration/login/login.component';
import { SkillDetailComponent } from './users/skill-detail/skill-detail.component';

const routes: Routes = [
  {path: 'users', component: UsersTableComponent},
  {path: 'users/add', component: AddUserComponent},
  {path: 'users/details/:id', component: DetailsComponent},
  {path: 'users/:id', component: AddUserComponent},
  {path: 'skills', component: SkillsTableComponent},
  {path: 'skills/details/:id', component: SkillDetailComponent},
  {path: 'skills/add', component: AddSkillComponent},
  {path: 'skills/:id', component: AddSkillComponent},
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegFormComponent},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  // path za registraciju: TO DO

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
