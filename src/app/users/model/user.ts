import { Skill } from "./skill";

export class User {
  UserId: number;
  Name: string;
  Email: string;
  Password: string;
  Picture: boolean;
  Photo: string;
  Skills: Array<Skill>;

  constructor(obj?: any) {
    this.UserId = obj && obj.UserId || null;
    this.Name = obj && obj.Name || "";
    this.Email = obj && obj.Email || "";
    this.Password = obj && obj.Password || "";
    this.Picture = obj && obj.Picture || false;
    this.Photo = obj && obj.Photo || "";
    this.Skills = obj && obj.Skills || [];
  }
}


