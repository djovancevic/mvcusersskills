import { User } from "./user";

export class Skill {
    SkillId: number;
    SkillName: string;
    Users: Array<User>;
  
    constructor(obj?: any) {
      this.SkillId = obj && obj.SkillId || null;
      this.SkillName = obj && obj.SkillName || "";
      this.Users = obj && obj.Users || [];
    }
  }