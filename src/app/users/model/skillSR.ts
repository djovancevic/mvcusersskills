import { Skill } from './skill';

export class SkillSR {
  Count: number;
  Items: Skill[];
  NextPageLink: string;

  constructor(obj?: any) {
    this.Count = obj && obj.Count || 0;
    this.Items = obj && obj.Items.map(elem => { return new Skill(elem); }) || [];
    this.NextPageLink = null;
  }
}