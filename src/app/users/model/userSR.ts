import { User } from './user';

export class UserSR {
  Count: number;
  Items: User[];
  NextPageLink: string;

  constructor(obj?: any) {
    this.Count = obj && obj.Count || 0;
    this.Items = obj && obj.Items.map(elem => { return new User(elem); }) || [];
    this.NextPageLink = null;
  }
}