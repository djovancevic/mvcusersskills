import { Component, OnInit, createPlatformFactory } from '@angular/core';
import { Skill } from '../model/skill';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'us-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.css']
})
export class AddSkillComponent implements OnInit {
  // tslint:disable-next-line:typedef-whitespace
  // tslint:disable-next-line:whitespace
  skill: Skill;
  skillForm: FormGroup;
  notifier: NotifierService;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private userService: UsersService, private router: Router, private activeRoute: ActivatedRoute, private notifierService: NotifierService) {
    this.createForm();
    this.notifier = notifierService;
   }

  ngOnInit() {
    // edit skill

     const id: string = this.activeRoute.snapshot.params.id;

     if (id) {
        this.userService.getSkill(Number(id)).subscribe(
          skill => {
            this.skill = skill;
            this.skillForm.patchValue(this.skill);
          }
        );
      }
  }

  createForm() {
    this.skillForm =  this.fb.group({
      SkillName : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]]
        });
  }

  onSubmit() {
    const submittedSkill: Skill = new Skill (this.skillForm.value);
    if (this.skill && this.skill.SkillId) {
      submittedSkill.SkillId = this.skill.SkillId;
      this.userService.updateSkill(submittedSkill).subscribe( skill => {
          this.skillForm.reset();
          this.router.navigate(['/skills']);
          this.notifier.notify('success', 'Skill is successfuly edited.');
          // console.log('Editovani user ima sledece skillove: ', user.Skills);
        });
    } else {

      this.userService.addSkill(submittedSkill).subscribe( skill => {
          this.skillForm.reset();
          this.router.navigate(['/skills']);
          this.notifier.notify('success', 'Skill is successfuly added.');
        });
    }
  }

  skillNameValidate(notyType: string, notyMessage: string): void {
    if (this.skillForm.get('SkillName').status === 'VALID') {
      this.notifier.notify('success', 'SkillName is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }

}
