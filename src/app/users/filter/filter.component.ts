import { Component, OnInit, Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter }  from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms'

@Component({
  selector: 'us-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @Output() onSearchByEmail :EventEmitter<string>;
  @Output() onSearchBySkill :EventEmitter<string>;
  searchFormByEmail :FormGroup;
  searchFormBySkill :FormGroup;
  @Input() flagEmail :number;
  @Input() flagSkill :number;

  constructor(private fb: FormBuilder) {
    this.onSearchByEmail = new EventEmitter();
    this.onSearchBySkill = new EventEmitter();
    // this.flagEmail = 0;
    // this.flagSkill = 0;

    this.searchFormByEmail = this.fb.group({
      'controlByEmail': ''
    });

    this.searchFormBySkill = this.fb.group({
      'controlBySkill': ''
    });



  }

  ngOnInit() { }

  ngOnChange(){
    if(this.flagEmail){
      this.flagSkill = 0;
    }else{
      this.flagEmail = 0;
    }
  }

  searchByEmail(): void {
    this.onSearchByEmail.emit(this.searchFormByEmail.value.controlByEmail);//onSearch je eventEmitter, searchForm je naziv grupe, a serachByEmail je naziv controle u grupi
  }

  searchBySkill(): void {
    this.onSearchBySkill.emit(this.searchFormBySkill.value.controlBySkill);
  }

}
