import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { UserSR } from '../model/userSR';
import { SkillSR } from '../model/skillSR';
import { User } from '../model/user';
import { Skill } from '../model/skill';
import { Credentials } from '../model/credentials';
import { UserUploadFile } from '../model/userUploadFile'
import { HttpHeaders } from '@angular/common/http'

const httpOptions = {
  headers: new HttpHeaders({
     'Accept': 'application/json'
  })
};
const usersUrl = 'http://localhost:64738/api/users';
const skillsUrl = 'http://localhost:64738/api/skills';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {}

  

  getAll(params?): Observable<UserSR> {
    let queryString = {};

    if ( params ) {
      queryString = {
        params : new HttpParams()
          .set('$orderby', params.$orderby || '')
          .set('$filter', params.$filter || '')
          .set('$top', params.$top || 10)
          .set('$skip', params.$skip || 0)
          .set('$inlinecount', params.$inlinecount || 'allpages')

      };

    }
    
    // UserSkillURL:    http://localhost:64738/api/users?$orderby=Name desc&$filter=substringof(' ', Name)&$top=10&skip=0&$inlinecount=allpages
    // Warehouse URL:    http://localhost:3000/api/documents?page=1&pageSize=10&sort=dateOfCreation&sortDirection=desc


    return this.http.get(usersUrl, queryString).pipe(map(response => {
        return new UserSR(response);
    }));
  }

  getAllSkills(params?): Observable<SkillSR> {
    let queryString = {};

    if (params) {
      queryString = {
        params : new HttpParams()
          .set('$orderby', params.$orderby || '')
          .set('$filter', params.$filter || '')
          .set('$top', params.$top || 10)
          .set('$skip', params.$skip || 0)
          .set('$inlinecount', params.$inlinecount || 'allpages')

      };
    }

    return this.http.get(skillsUrl, queryString).pipe(map(response => {
        return new SkillSR(response);
    }));
  }

  addUser(formData: FormData ): Observable<User> {
    
    return this.http.post(usersUrl, formData, httpOptions).pipe(map(resp => {
      return new User(resp);
    }));
  }

  addSkill(skill: Skill): Observable<Skill> {
    skill.SkillId = 0;
    return this.http.post(skillsUrl, skill).pipe(map(resp => {
      return new Skill(resp);
    }));
  }

  removeUser(id: number): Observable<User> {
    return this.http.delete(usersUrl + '/' + id).pipe(map(
      response => new User(response)
    ));
  }
  // DELETE api/users/{id}
  removeSkill(id: number): Observable<Skill> {
    return this.http.delete(skillsUrl + '/' + id).pipe(map(
      response => new Skill(response)
    ));
  }

  getUser(id: number): Observable<User> {
    return this.http.get(usersUrl + '/' + id).pipe(map(
      response => new User(response)
    ));
  }

  getSkill(id: number): Observable<Skill> {
    return this.http.get(skillsUrl + '/' + id).pipe(map(
      response => new Skill(response)
    ));
  }

  updateUser(formData : FormData): Observable<User> {
    let editedUserId : number = Number.parseInt(formData.get('UserId').toString())
    return this.http.put(usersUrl + '/' + editedUserId, formData, httpOptions).pipe(map(
      response => new User(response)
    ));
  }

  updateSkill(editedSkill: Skill): Observable<Skill> {
    return this.http.put(skillsUrl + '/' + editedSkill.SkillId, editedSkill).pipe(map(
      response => new Skill(response)
    ));
  }

  getPhoto(user: User): Observable<Blob> {
    console.log(user.Photo);
    if (user.Photo) {
      return this.http.get( 'http://localhost:64738/' + user.Photo, { responseType : 'blob' });
    } else {
     console.log('Service ne dobija photoUrl.');
    }
  }

  

  // registracija se obavlja na serveru i to je post metod kojim se salju vrednosti register forme user, pass i confirmPass
  registerUser(credentials: Credentials) {
    return this.http.post('http://localhost:64738/api/Account/register', credentials);
  }


}
