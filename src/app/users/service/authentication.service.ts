import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
    
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {

      var userData ="grant_type=password" + "&username=" + username + "&password=" + password ;
      console.log(userData)
      var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post<any>('http://localhost:64738/Token', userData, { headers: reqHeader })
            .pipe(map(credentials => {
                // login successful if there's a jwt token in the response
                if (credentials && credentials.access_token) {
                    console.log("Ovo je poruka ako ima access_token ", JSON.stringify(credentials));
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(credentials));
                    
                }
                
                return credentials;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        console.log("Current user je izbrisan ", localStorage.getItem('currentUser'));
        localStorage.clear();
    }
}