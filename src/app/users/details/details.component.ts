import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../service/users.service';
import { User } from '../model/user';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'us-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  user: User;
  photoToShow: any;
  isPhotoLoading: boolean;

  constructor(private userService: UsersService, private activeRoute: ActivatedRoute) {
    this.isPhotoLoading = true;
    this.user = new User();
   }

  ngOnInit() {

    // user details
    const id: string = this.activeRoute.snapshot.params.id;

    if (id) {
      this.userService.getUser(Number(id)).subscribe(
        user => {
          this.user = user;
          console.log('User name: ', this.user.Name, ' photoUrl: ', this.user.Photo);
          this.getPhotoService();
        }
      );
    }
  }

    // loaduje image/jpeg kao dogadjaj pomocu FileReadera na osnovu Observable<Blob> koji dobija iz getPhotoService
    createPhotoFromBlob(photo: Blob) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.photoToShow = reader.result; // reader.result je string
      }, false); // false je opcioni parametar koji sledi nakon lambda fn

      if (photo) {
        reader.readAsDataURL(photo);
      }

    }


    getPhotoService() {
      this.isPhotoLoading = true;
      this.userService.getPhoto(this.user).subscribe(data => {
        this.createPhotoFromBlob(data);
        this.isPhotoLoading = false;
      }, error => {
        this.isPhotoLoading = true;
        console.log('Dogodila se greska prilikom poziva servisa', error.statusText);
      });
    }


  }






