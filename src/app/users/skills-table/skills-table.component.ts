import { Component, OnInit } from '@angular/core';
import { Skill } from '../model/skill';
import { SkillSR } from '../model/skillSR';
import { UsersService } from '../service/users.service';
import { Router } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'us-skills-table',
  templateUrl: './skills-table.component.html',
  styleUrls: ['./skills-table.component.css']
})
export class SkillsTableComponent implements OnInit {
  public skills: SkillSR;
  flagSkill: number;
  // for paging component
  collectionSize: number;
  page :number;


  params = {
    $orderby : 'SkillName',
    $filter : 'substringof(\'\', SkillName)',
    $top : 5,
    $skip : 0,
    $inlinecount : 'allpages'
  };

  constructor(private skillService: UsersService,
              private router: Router,
              private ngbService: NgbPaginationConfig,) {
    this.flagSkill = 1;
    this.collectionSize = 0;
    }

  ngOnInit() {
    this.updateSkills();
    this.ngbService.pageSize = this.params.$top; // pageSize u ngb se podesava preko ngbService
  }

  updateSkills() {
    this.skillService.getAllSkills(this.params).subscribe( skillSR =>{ 
      this.collectionSize = skillSR.Count;
      this.ngbService.maxSize = Math.ceil(this.collectionSize / this.params.$top); // maxSize je u ngb NumberOfPages
      this.skills = skillSR});
  }

  onDelete(id: number) {
    this.skillService.removeSkill(id).subscribe(
      skill => { this.updateSkills(); });
  }

  onDetails(skill: Skill) {

      this.router.navigate(['skills/details/', skill.SkillId]);

  }

  onSort(criteria: string) {
		if (this.params.$orderby === criteria) {
		
			
			if (this.params.$orderby.includes('desc')) {
				this.params.$orderby = criteria;
			} else {
				this.params.$orderby = criteria + ' desc';
			}
		} else {
			this.params.$orderby = criteria;
    }
		this.updateSkills();
  }

  searchBySkill(searchString: string) {
    this.params.$filter = 'substringof(\'' + searchString + '\', SkillName)';
    this.updateSkills();
  }

  pageChange(selectedPage: number) {
    this.params.$skip = (selectedPage - 1) * this.params.$top;
		  this.updateSkills();
  }


}
