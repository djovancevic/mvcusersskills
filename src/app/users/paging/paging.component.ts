import { Component, OnInit, Input, OnChanges, Output } from '@angular/core';
import { EventEmitter } from "@angular/core"

@Component({
  selector: 'us-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.css']
})
export class PagingComponent implements OnInit, OnChanges {
  
  @Input() collectionSize :number;
  @Output() onPageSelected :EventEmitter<number>;
  
  
  page :number;

  constructor() { 
    this.onPageSelected = new EventEmitter();
    this.page = 1;
  }

  ngOnInit() {
  }

  ngOnChanges(){
    console.log("Iz PagingComp ngOnChange collectionSize= ", this.collectionSize);
  }

  pageChange(){
    this.onPageSelected.emit(this.page);
  }

}
