import { Component, OnInit } from '@angular/core';
import { UserSR } from '../model/userSR';
import { User } from '../model/user';
import { UsersService } from '../service/users.service';
import { Router } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { NotifierService } from 'angular-notifier';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'us-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {

  public users: User[] = []; // objekat tipa SearchResult: count + User[]
  public count = 0;
  private checkCount: number;
  flagEmail: number;
  notifier: NotifierService;



  // server sortira na osnovu $orderby ako je samo criterium onda je asscending ;
  // server filtrira na osnovu $filter tako sto mu se menja searchString i polje modela;
  //
  params = {
    $orderby : 'Name desc',
    $filter : 'substringof(\'\', Name)',
    $top : 5,
    $skip : 0, // skip se podesava @Output parametrima iz page comp po obrascu: (page - 1) * pageSize
    $inlinecount : 'allpages'
  };

  // tslint:disable-next-line:max-line-length
  constructor(private userService: UsersService,
              private router: Router,
              private ngbService: NgbPaginationConfig,
              private location: Location,
              private nofierService: NotifierService) {
    this.flagEmail = 0;


    }

  ngOnInit() {
    this.refreshUsers();
    // @Input parametar collectionSize  = count
    this.checkCount = this.count;
    this.ngbService.pageSize = this.params.$top; // pageSize u ngb se podesava preko ngbService
    console.log('Iz UsersTableComp ngOnInit   Count= ', this.count, '   maxSize=', this.ngbService.maxSize );
   }

  refreshUsers() {
    this.userService.getAll(this.params).subscribe(usersSR => {
      this.count = usersSR.Count;
      this.ngbService.maxSize = Math.ceil(this.count / this.params.$top); // maxSize je u ngb NumberOfPages
      console.log('Iz UserTableComp ngOnInit-RefreshUsers   Count= ', this.count, '   maxSize=', this.ngbService.maxSize );
      this.users = usersSR.Items;
      if (this.count !== this.checkCount) {
        this.checkCount = this.count;
        this.location.go('users');
      }
      } );
  }

  onDelete(id: number): void {
    this.userService.removeUser(id).subscribe(data => {
      this.refreshUsers();
      this.notifier.notify('success', 'User is successfuly deleted');
    });
  }

  onDetails(user: User) {
    this.router.navigate(['users/details/', user.UserId]);

  }

  onSort(criteria: string) {
		// tslint:disable-next-line:indent
		// tslint:disable-next-line:indent
		// tslint:disable-next-line:indent
		if (this.params.$orderby === criteria) {
			// tslint:disable-next-line:indent
			if (this.params.$orderby.includes('desc')) {
				this.params.$orderby = criteria;
			} else {
				this.params.$orderby = criteria + ' desc';
			}
		} else {
			this.params.$orderby = criteria;
    }
		this.refreshUsers();
  }

  searchByEmail(searchString: string) {
    this.params.$filter = 'substringof(\'' + searchString + '\', Email)';
    this.refreshUsers();
  }

  onPageSelected(selectedPage: number) {
    this.params.$skip = (selectedPage - 1) * this.params.$top;
		  this.refreshUsers();
  }




  // onEdit(id :number) :void{
  //   this.router.navigate(['/users/add', id]);
  // }

}
