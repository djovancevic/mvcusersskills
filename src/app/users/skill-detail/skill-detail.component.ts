import { Component, OnInit } from '@angular/core';
import { Skill } from '../model/skill';
import { UsersService } from '../service/users.service';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'us-skill-detail',
  templateUrl: './skill-detail.component.html',
  styleUrls: ['./skill-detail.component.css']
})
export class SkillDetailComponent implements OnInit {

  skill: Skill;

  constructor(private skillService: UsersService, private activeRoute: ActivatedRoute, private notifierService: NotifierService) { 
    this.skill = new Skill(); 
  }

  ngOnInit() {
    // skill details
    const id: string = this.activeRoute.snapshot.params.id;

    if (id) {
      this.skillService.getSkill(Number(id)).subscribe(
        skill => {
          this.skill = skill;
          this.notifierService.notify('success', 'Skill is successfuly got.');
          
        }
      );
    }
  }

}
