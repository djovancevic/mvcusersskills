import { Component, OnInit } from '@angular/core';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { User } from '../model/user';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Skill } from '../model/skill';
import { Input } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { VALID, INVALID } from '@angular/forms/src/model';
import { UserUploadFile } from '../model/UserUploadFile';
import { RxFormGroup, RxFormBuilder, FormGroupExtension, RxwebValidators } from '@rxweb/reactive-form-validators';

// import { NgNoty, NotyOptions } from 'ng-noty';
export interface data {

  Photo:string;
}


@Component({
  // tslint:disable-next-line:component-selector 
  selector: 'us-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
  providers: [UsersService]
})
export class AddUserComponent implements OnInit {

  user: User = new User ();
  userForm: FormGroup;
  skills: Skill[] = [{ SkillId: 3, SkillName: 'Web Designer', Users: [] }];
  selectedSkill: string;
  // za potrebe notifikacije
  private notifier: NotifierService;

  


  params = {
    $orderby : 'SkillName',
    $filter : 'substringof(\'\', SkillName)',
    $top : 10,
    $skip : 0,
    $inlinecount : 'allpages'
  };

  constructor(private skillService: UsersService, private fb: FormBuilder, private userService: UsersService,
              private router: Router, private activeRoute: ActivatedRoute, private notifierService: NotifierService) {
    this.createForm();
    //this.SetForm(this.dat);
    this.notifier = notifierService;
   }

  ngOnInit() {
     this.getSkills();
     // edit user
     const id: string = this.activeRoute.snapshot.params.id;

     if (id) {
        this.userService.getUser(Number(id)).subscribe(
          user => {
            this.user = user;
            this.userForm.patchValue(this.user);
          }
        );
      } else{
        this.user.UserId = 0;
      }

      // preporuka je da se Skills objekat prebaci u niz id-ova radi lakseg mapiranja
      // let skills = [1,3];
      // this.user = new User({UserId:1,Name:"Petar", Skills:skills});
      // this.userForm.patchValue(this.user);

  }

  createForm() {
    this.userForm =  this.fb.group({
      Name : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      Email : ['', [Validators.required, Validators.email, Validators.minLength(6)]],
      Password : ['', [Validators.required, Validators.pattern('(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}')]],
      Photo : ['', Validators.required],
      Skills : [[[{}]], Validators.required ],   // ovo treba da je select-box element koji prima niz skill objekata

    

    });
  }

  

  getSkills() {
    this.skillService.getAllSkills(this.params).subscribe( data => {
      this.skills = data.Items;
      
      for (let i = 0; i < this.skills.length; i++) {
        this.skills[i].Users = [];
        }
      return this.skills;
    });

  }

  onSubmit() {
    const submittedUser: any = this.userForm.value;
    this.userForm.addControl('UserId', new FormControl(''));
    this.userForm.addControl('Picture', new FormControl(''))
    // creating multipart/form-data
    let formData = new FormData();
    this.userForm.get('UserId').setValue(this.user.UserId);
    this.userForm.get('Picture').setValue(this.user.Picture);
    formData.append('UserId', this.userForm.get('UserId').value );
    formData.append('Name', this.userForm.get('Name').value);
    formData.append('Email', this.userForm.get('Email').value);
    formData.append('Password', this.userForm.get('Password').value);
    formData.append('Photo', this.userForm.get('Photo').value);
    formData.append('Picture', this.userForm.get('Picture').value);
    formData.append('Skills', JSON.stringify(this.userForm.get('Skills').value)); // array has to be stringify

    if (this.user && this.user.UserId) {
      this.userService.updateUser(formData).subscribe( user => {
          this.userForm.reset();
          this.router.navigate(['/users']);
          this.notifier.notify('success', 'User is successfuly edited.');
          // console.log('Editovani user ima sledece skillove: ', user.Skills);
        });
    } else {
      this.userService.addUser(formData).subscribe( user => {
          this.userForm.reset();
          this.router.navigate(['/users']);
          this.notifier.notify('success', 'User is successfuly added.');
          // console.log('Na serveru je kreiran novi user sa skilovima: ', user.Skills);
        });
    }


    
  }

  
  
  compareFn(c1: any, c2: any): boolean {
    let flag: boolean;
    
    return  flag = c1 && c2 ? c1.SkillId === c2.SkillId : c1 === c2; 
    // ako je c1&&c2 = true (a uvek jeste jer su truthy) onda je flag = boolean vrednosti levo od :; izraz c1===c2 je uvek false.
  }

  // angular-notifier
  /*public showNotification(type :string, message :string) :void{
      this.notifier.notify(type, message);
  }*/

  

  nameValidate(notyType: string, notyMessage: string): void {
    if (this.userForm.get('Name').status === 'VALID') {
      this.notifier.notify('success', 'Name is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }

  emailValidate(notyType: string, notyMessage: string): void {
    if (this.userForm.get('Email').status === 'VALID') {
      this.notifier.notify('success', 'Email is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }

  passwordValidate(notyType: string, notyMessage: string): void {
    if (this.userForm.get('Password').status === 'VALID') {
      this.notifier.notify('success', 'Password is entered correctly');
    } else {
      this.notifier.notify(notyType, notyMessage);
    }
  }

  photoInfo(notyType: string, notyMessage: string): void {
    this.notifier.notify(notyType, notyMessage);
  }

  // image upload: when image is insert into input type=file this method is called to set fileToUpload prop
  handleFileInput(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.userForm.get('Photo').setValue(file);
      this.user.Picture = true;

    }
    
    
  }

}
